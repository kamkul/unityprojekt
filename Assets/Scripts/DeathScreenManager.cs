﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DeathScreenManager : MonoBehaviour
{
    public Canvas deathScreen;
    public Canvas scoreScreen;
    public GameObject pointsText;
    public GameObject points;
    private Slider slider;

    private void Start()
    {
        slider = scoreScreen.GetComponentInChildren<Slider>();
    }

    void FixedUpdate()
    {
        CheckSliderValue();
    }

    private void CheckSliderValue()
    {
        if (slider.value <= 0)
        {
            Time.timeScale = 0;
            scoreScreen.gameObject.SetActive(false);
            deathScreen.gameObject.SetActive(true);

            pointsText.GetComponent<TextMeshProUGUI>().text = points.GetComponent<Text>().text;
        }
    }
}
