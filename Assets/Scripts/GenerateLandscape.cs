﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GenerateLandscape : MonoBehaviour
{
    public GameObject landscapeHolder;
    public GameObject player;
    public GameObject landscape;
    public GameObject landscapeEmpty;
    private int landscapeStep;
    public Dictionary<int, List<GameObject>> createdlandscapeAtStep = new Dictionary<int, List<GameObject>>();
    public int viewDistance = 10;
    public int viewDistanceX = 5;
    public int landscapeStepLength = 164;
    public int landscapeHeight = 0;
    private int terrainStep;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        landscapeStep = Mathf.FloorToInt(player.transform.position.z / landscapeStepLength);

        for (int i = 0; i < viewDistance; i++)
        {
            int landscapeStepForward = (landscapeStep + i + 1);
            int landscapeStepBackward = (landscapeStep - i);

            if (!createdlandscapeAtStep.ContainsKey(landscapeStepForward))
            {
                List<GameObject> createdLandscapes = new List<GameObject>();
                for (int j = -viewDistanceX + 1; j < viewDistanceX; j++)
                {
                    GameObject gameObject;
                    if (j == 0)
                    {
                        gameObject = Instantiate(landscapeEmpty, new Vector3(j * landscapeStepLength, landscapeHeight, landscapeStepForward * landscapeStepLength), Quaternion.identity);
                    }
                    else
                    {
                        gameObject = Instantiate(landscape, new Vector3(j * landscapeStepLength, landscapeHeight, landscapeStepForward * landscapeStepLength), Quaternion.identity);
                    }
                    createdLandscapes.Add(gameObject);
                    gameObject.transform.parent = landscapeHolder.transform;
                }
                createdlandscapeAtStep.Add(landscapeStepForward, createdLandscapes);
            }

            if (!createdlandscapeAtStep.ContainsKey(landscapeStepBackward))
            {
                List<GameObject> createdLandscapes = new List<GameObject>();
                for (int j = -viewDistanceX + 1; j < viewDistanceX; j++)
                {
                    GameObject gameObject;
                    if (j == 0)
                    {
                        gameObject = Instantiate(landscapeEmpty, new Vector3(j * landscapeStepLength, landscapeHeight, landscapeStepBackward * landscapeStepLength), Quaternion.identity);

                    }
                    else
                    {
                        gameObject = Instantiate(landscape, new Vector3(j * landscapeStepLength, landscapeHeight, landscapeStepBackward * landscapeStepLength), Quaternion.identity);
                    }
                    createdLandscapes.Add(gameObject);
                    gameObject.transform.parent = landscapeHolder.transform;
                }
                createdlandscapeAtStep.Add(landscapeStepBackward, createdLandscapes);
            }
        }

        foreach (var elem in createdlandscapeAtStep.Where(kv => !(kv.Key <= (landscapeStep + 15 + 1) && kv.Key >= (landscapeStep - 15))).ToList())
        {
            foreach (var landscape in elem.Value)
            {
                Destroy(landscape.gameObject);
            }
            createdlandscapeAtStep.Remove(elem.Key);
        }

        //foreach (var elem in createdlandscapeAtStep)
        //{
        //    if (!(elem.Key <= (landscapeStep + landscapeStepLength / 2 + 1) && elem.Key > (landscapeStep - landscapeStepLength / 2)))
        //    {
        //        Destroy(elem.Value.gameObject);
        //        createdlandscapeAtStep.Remove(elem.Key);
        //    }
        //}
    }
}
