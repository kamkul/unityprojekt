﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectClock : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject car;
    public GameObject clockMeter;
    public int maxClockMeter = 15;
    private Slider slider;

    private void Start()
    {
        slider = clockMeter.GetComponent<Slider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "CollactableClock")
        {
            Debug.Log("Collected clock");
            slider.value = maxClockMeter;
            Destroy(other.gameObject);
        }
    }
}
