﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class NpcCarCreator : MonoBehaviour
{
    public GameObject player;
    public List<GameObject> npcCars;
    public int numberOfnpcCars;
    public List<GameObject> createdNpcCars;
    public int npcCarDestroyRange;
    public int pointZeroOfCreateCars = 240;
    public int currentTerrainStep;
    public int npcCarStartCreatingDistance = 2;
    public int terrainStepLength;
    public GameObject terrainHolder;
    public GameObject currentTerrain;
    public List<Transform> nodes = new List<Transform>();

    private void Start()
    {
        terrainHolder = GameObject.FindGameObjectsWithTag("TerrainHolder")[0];
        terrainStepLength = terrainHolder.GetComponent<GenerateTerrain>().terrainStepLength;
    }

    // Update is called once per frame
    void Update()
    {
        DestroyFarCars();

        int tmpCurrentTerrainStep = Mathf.FloorToInt(player.transform.position.z / terrainStepLength);
        if (tmpCurrentTerrainStep != currentTerrainStep)
        {
            currentTerrainStep = tmpCurrentTerrainStep;
            CreateCars();
        }
    }

    private void DestroyFarCars()
    {
        List<GameObject> carsToDestroy = new List<GameObject>();

        foreach (GameObject car in createdNpcCars)
        {
            if ((player.transform.position.z - car.transform.position.z) > npcCarDestroyRange / 6 ||
                (car.transform.position.z - player.transform.position.z) > npcCarDestroyRange)
            {
                carsToDestroy.Add(car);
            }
        }

        foreach (GameObject car in carsToDestroy)
        {
            Destroy(car);
        }

        createdNpcCars = createdNpcCars.Except(carsToDestroy).ToList();
    }

    private void CreateCars()
    {
        terrainHolder.GetComponent<GenerateTerrain>().createdTerrainAtStep.TryGetValue(currentTerrainStep + npcCarStartCreatingDistance, out currentTerrain);
        if (currentTerrain != null)
        {
            Transform path = currentTerrain.transform.Find("Path").transform;
            List<Transform> nodes = new List<Transform>();
            Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();

            for (int j = 0; j < pathTransforms.Length; j += 3)
            {
                if (pathTransforms[j] != path)
                {
                    nodes.Add(pathTransforms[j]);
                }
            }

            for (int i = 0; i < nodes.Count && createdNpcCars.Count < numberOfnpcCars; i++)
            {
                Vector3 spawnPoint = new Vector3(
                    nodes[i].position.x + 2,
                    nodes[i].position.y,
                    nodes[i].position.z
                    );

                GameObject createdCar = Instantiate(npcCars[Random.Range(0, npcCars.Count)], spawnPoint, Quaternion.identity);
                createdCar.GetComponent<CarEngine>().terrainHolder = terrainHolder;
                createdNpcCars.Add(createdCar);
            }
        }
    }
}
