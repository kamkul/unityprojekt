﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClockHandler : MonoBehaviour
{
    public Slider clockMeter;
    public Gradient gradient;
    public Image fill;
    public int maxTime = 15;
    public float multiplier = 0.7f;

    void Start()
    {
        clockMeter.value = maxTime;
        fill.color = gradient.Evaluate(1f);
    }

    void SetMeter()
    {
        clockMeter.value -= Time.deltaTime * multiplier;
        fill.color = gradient.Evaluate(clockMeter.normalizedValue);
    }

    // Update is called once per frame
    void Update()
    {
        SetMeter();
    }
}
