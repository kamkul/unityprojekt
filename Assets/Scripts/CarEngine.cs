﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CarEngine : MonoBehaviour
{
    public float turnSpeed = 5f;
    public GameObject car;
    public int currentTerrainStep;
    public Transform path;
    public Transform nextPath;
    public float maxSteerAngle = 45f;
    public WheelCollider wheelFL;
    public WheelCollider wheelFR;
    public WheelCollider wheelRL;
    public WheelCollider wheelRR;
    public float maxMotorTorque = 80f;
    public float maxBrakeTorque = 150f;
    public float currentSpeed;
    public float maxSpeed = 100f;
    public bool isBraking = false;
    public GameObject terrainHolder;
    private int terrainStepLength;
    private GameObject currentTerrain;
    private GameObject nextCurrentTerrain;
    public Transform[] pathTransforms;
    public Transform[] nextPathTransforms;
    private float targetSteerAngle = 0;

    [SerializeField] private Transform frontLeftWheelTransform;
    [SerializeField] private Transform frontRightWheeTransform;
    [SerializeField] private Transform rearLeftWheelTransform;
    [SerializeField] private Transform rearRightWheelTransform;

    public List<Transform> nodes;
    private int currectNode = 0;

    private void Start()
    {
        terrainStepLength = terrainHolder.GetComponent<GenerateTerrain>().terrainStepLength;
    }

    private void FixedUpdate()
    {
        UpdateCurrentTerrain();
        VerifyCheckPoint();
        ApplySteer();
        Drive();
        CheckWaypointDistance();
        Braking();
        LerpToSteerAngle();
        UpdateWheels();
    }

    private void UpdateWheels()
    {
        UpdateSingleWheel(wheelFL, frontLeftWheelTransform);
        UpdateSingleWheel(wheelFR, frontRightWheeTransform);
        UpdateSingleWheel(wheelRR, rearRightWheelTransform);
        UpdateSingleWheel(wheelRL, rearLeftWheelTransform);
    }

    private void UpdateSingleWheel(WheelCollider wheelCollider, Transform wheelTransform)
    {
        Vector3 pos;
        Quaternion rot;
        wheelCollider.GetWorldPose(out pos, out rot);
        wheelTransform.rotation = rot;
        wheelTransform.position = pos;
    }

    private void VerifyCheckPoint()
    {
        if (nodes.Count > currectNode)
            if (nodes[currectNode].transform.position.z < car.transform.position.z)
            {
                currectNode++;
            }
    }

    private void UpdateCurrentTerrain()
    {
        int tmpCurrentTerrainStep = Mathf.FloorToInt(car.transform.position.z / terrainStepLength);
        if (currentTerrainStep != tmpCurrentTerrainStep || Equals(currentTerrainStep, default(int)))
        {
            Debug.Log("Updated currentTerrainStep");
            terrainHolder.GetComponent<GenerateTerrain>().createdTerrainAtStep.TryGetValue(tmpCurrentTerrainStep, out currentTerrain);
            terrainHolder.GetComponent<GenerateTerrain>().createdTerrainAtStep.TryGetValue(tmpCurrentTerrainStep + 1, out nextCurrentTerrain);

            path = currentTerrain.transform.Find("Path").transform;
            nextPath = nextCurrentTerrain.transform.Find("Path").transform;

            currentTerrainStep = tmpCurrentTerrainStep;

            pathTransforms = path.GetComponentsInChildren<Transform>();
            nextPathTransforms = nextPath.GetComponentsInChildren<Transform>();
            nodes = new List<Transform>();

            for (int i = 0; i < pathTransforms.Length; i++)
            {
                if (pathTransforms[i] != path)
                {
                    nodes.Add(pathTransforms[i]);
                }
            }

            for (int i = 0; i < nextPathTransforms.Length; i++)
            {
                if (nextPathTransforms[i] != nextPath)
                {
                    nodes.Add(nextPathTransforms[i]);
                }
            }

            currectNode = 0;
        }
    }

    private void ApplySteer()
    {
        Vector3 relativeVector = transform.InverseTransformPoint(nodes[currectNode].position);
        float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;
        targetSteerAngle = newSteer;
    }

    private void Drive()
    {
        currentSpeed = 2 * Mathf.PI * wheelFL.radius * wheelFL.rpm * 60 / 1000;

        if (currentSpeed < maxSpeed && !isBraking)
        {
            wheelFL.motorTorque = maxMotorTorque;
            wheelFR.motorTorque = maxMotorTorque;
        }
        else
        {
            wheelFL.motorTorque = 0;
            wheelFR.motorTorque = 0;
        }
    }

    private void CheckWaypointDistance()
    {
        if (Vector3.Distance(transform.position, nodes[currectNode].position) < 1f)
        {
            if (currectNode == nodes.Count - 1)
            {
                currectNode = 0;
            }
            else
            {
                currectNode++;
            }
        }
    }

    private void Braking()
    {
        if (isBraking)
        {
            wheelRL.brakeTorque = maxBrakeTorque;
            wheelRR.brakeTorque = maxBrakeTorque;
        }
        else
        {
            wheelRL.brakeTorque = 0;
            wheelRR.brakeTorque = 0;
        }
    }
    private void LerpToSteerAngle()
    {
        wheelFL.steerAngle = Mathf.Lerp(wheelFL.steerAngle, targetSteerAngle, Time.deltaTime * turnSpeed);
        wheelFR.steerAngle = Mathf.Lerp(wheelFR.steerAngle, targetSteerAngle, Time.deltaTime * turnSpeed);
    }
}
