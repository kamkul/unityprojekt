﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsHandler : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject pointsMeter;
    public GameObject player;
    private Text text;
    public int points;
    public int pointsOffset;

    private void Start()
    {
        text = pointsMeter.GetComponent<Text>();
        pointsOffset = (int)(player.transform.position.z / 10);
    }

    // Update is called once per frame
    void Update()
    {
        int playerPositionZ = (int)(player.transform.position.z / 10) - pointsOffset;
        if (Equals(points, default(int)) || playerPositionZ > points)
        {
            if (text != null)
            {
                points = playerPositionZ;
                text.text = "Points:" + points;
            }
        }

    }
}
