﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateClock : MonoBehaviour
{
    public int rotateMultiplier = 1;

    void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * rotateMultiplier);
    }
}
