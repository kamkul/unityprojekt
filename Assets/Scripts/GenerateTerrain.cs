﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GenerateTerrain : MonoBehaviour
{
    public GameObject terrainHolder;
    public GameObject player;
    public List<GameObject> terrainsList = new List<GameObject>();
    private int terrainStep;
    public Dictionary<int, GameObject> createdTerrainAtStep = new Dictionary<int, GameObject>();
    public int viewDistance = 10;
    public int terrainStepLength = 164;
    public int terrainHeight = 0;
    public int terrainX = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        terrainStep = Mathf.FloorToInt(player.transform.position.z / terrainStepLength);

        for (int i = 0; i < viewDistance; i++)
        {
            int terrainStepForward = (terrainStep + i + 1);
            int terrainStepBackward = (terrainStep - i);

            if (!createdTerrainAtStep.ContainsKey(terrainStepForward))
            {
                GameObject gameObject = Instantiate(terrainsList[Random.Range(0,terrainsList.Count)], new Vector3(terrainX, terrainHeight, terrainStepForward * terrainStepLength), Quaternion.identity);
                createdTerrainAtStep.Add(terrainStepForward, gameObject);
                gameObject.transform.parent = terrainHolder.transform;
            }

            if (!createdTerrainAtStep.ContainsKey(terrainStepBackward))
            {
                GameObject gameObject = Instantiate(terrainsList[Random.Range(0, terrainsList.Count)], new Vector3(terrainX, terrainHeight, terrainStepBackward * terrainStepLength), Quaternion.identity);
                createdTerrainAtStep.Add(terrainStepBackward, gameObject);
                gameObject.transform.parent = terrainHolder.transform;
            }
        }

        foreach (var elem in createdTerrainAtStep.Where(kv => !(kv.Key <= (terrainStep + 15 + 1) && kv.Key >= (terrainStep - 15))).ToList())
        {
            Destroy(elem.Value.gameObject);
            createdTerrainAtStep.Remove(elem.Key);
        }

        //foreach (var elem in createdTerrainAtStep)
        //{
        //    if (!(elem.Key <= (terrainStep + terrainStepLength / 2 + 1) && elem.Key > (terrainStep - terrainStepLength / 2)))
        //    {
        //        Destroy(elem.Value.gameObject);
        //        createdTerrainAtStep.Remove(elem.Key);
        //    }
        //}
    }
}
